import pyglet, pyglet.gl, math, random

batch = pyglet.graphics.Batch()
line_group = pyglet.graphics.OrderedGroup(0)
field_group = pyglet.graphics.OrderedGroup(1)
label_group = pyglet.graphics.OrderedGroup(2)
gameover_group = pyglet.graphics.OrderedGroup(3)

class Border(object):
	def __init__(self, width, height):
		self.x = 0
		self.y = 0
		self.mx = self.x + width
		self.my = self.y + height

class FinalText(object):
	def __init__(self, parent, type):
		self.parent = parent
		if type == "FAIL":
			self.text = "GAME OVER!"
			self.color = (255,0,0,255)
		if type == "WIN":
			self.text = "YOU'VE WON!"
			self.color = (0,255,0,255)
			
		self.title = pyglet.text.Label(self.text,
								font_name="Times New Roman",
								font_size=25,
								x=self.parent.width/2, y=self.parent.height/2,
								anchor_x="center", anchor_y="center",
								color=self.color,
								bold=True,
								batch=batch,
								group=gameover_group)
		self.score = pyglet.text.Label("SCORE: %d" % self.parent.score,
								font_name="Times New Roman",
								font_size=25,
								x=self.parent.width/2, y=(self.parent.height/2) - 50,
								anchor_x="center", anchor_y="center",
								color=self.color,
								bold=True,
								batch=batch,
								group=gameover_group)
		
class Line(object):
	def __init__(self, x, y):
		self.start_x = x
		self.start_y = y
		self.final_x = x
		self.final_y = y
		self.line_batch = pyglet.graphics.Batch()
		self.line_batch.add(2, pyglet.gl.GL_LINES,line_group,('v2i', (self.start_x, self.start_y, self.final_x, self.final_y)),('c3B', (0, 255, 0, 255, 0, 0)))
		
	def update(self, x, y):
		self.final_x = x
		self.final_y = y
		del self.line_batch
		self.line_batch = pyglet.graphics.Batch()
		self.line_batch.add(2, pyglet.gl.GL_LINES,line_group,('v2i', (self.start_x, self.start_y, self.final_x, self.final_y)),('c3B', (0, 255, 0, 255, 0, 0)))
		
	def stop(self):
		del self

class Rider(object):
	def __init__(self, parent):
		self.parent = parent
		self.x = 0
		self.y = 0
		self.rgb = (255,255,255)
		self.image = pyglet.image.load("./images/rider.png")
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch, group=field_group)
		self.sprite.color = self.rgb
		self.height = self.sprite.height
		self.width = self.sprite.width
		self.move((self.parent.width/2) - (self.sprite.width/2))
	
	def move(self, vx):
		self.x += vx
		if self.x < self.parent.border.x:
			self.x = self.parent.border.x
		if self.x + self.width > self.parent.border.mx:
			self.x = self.parent.border.mx - self.width
		self.sprite.x = self.x
	

class Field(object):
	def __init__(self, x, y, type, parent):
		self.x = x
		self.y = y
		self.parent = parent
		
		if type == 1:
			self.score = 10
			self.rgb = (0,255,0)
		elif type == 2:
			self.score = 20
			self.rgb = (0,0,255)
		else:
			self.score = 0
			self.rgb = (100,100,100)
		
		self.image = pyglet.image.load("./images/field.png")
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch, group=field_group)
		self.sprite.color = self.rgb
		self.height = self.sprite.height
		self.width = self.sprite.width
		self.label = pyglet.text.Label(str(self.score),
								font_name="Times New Roman",
								font_size=15,
								x=self.x+(self.width/2), y=self.y+(self.height/2),
								anchor_x="center", anchor_y="center",
								color=(255,255,255,255),
								bold=True,
								batch=batch,
								group=label_group)
	
	def hit(self):
		self.sprite.delete()
		self.label.delete()
		self.parent.remove_field(self)
		self.parent.add_to_score(self.score)

class Ball(object):
	def __init__(self, xy, vxy, parent):
		self.radius = 10
		self.diameter = self.radius * 2
		self.x = xy[0] - self.radius
		self.y = xy[1] - self.radius
		self.vx = vxy[0]
		self.vy = vxy[1]
		self.rgb = (255,0,0)
		self.parent = parent
		self.previous_x = None
		self.previous_y = None
		
		self.correction = 1.0
		
		self.image = pyglet.image.load("./images/ball.png")
		self.sprite = pyglet.sprite.Sprite(self.image, self.x, self.y, batch=batch)
		self.sprite.color = self.rgb
	
	def move(self):
		self.previous_x = self.x
		self.previous_y = self.y
		self.x += self.vx
		self.y += self.vy
		
		self.check_rider_collision()
		self.check_border_collision()
		self.check_field_collision()
		
		self.sprite.x = self.x
		self.sprite.y = self.y
	
	def check_rider_collision(self):
		if self.y < self.parent.rider.y + self.parent.rider.height and self.previous_y > self.parent.rider.y + self.parent.rider.height:
		#if self.y < self.parent.rider.y + self.parent.rider.height:
			if self.x + (self.diameter * 0.66) > self.parent.rider.x and (self.x + self.diameter) - (self.diameter * 0.66) < self.parent.rider.x + self.parent.rider.width:
				self.vy = (self.vy * -1) * self.correction
				self.y = self.parent.rider.y + self.parent.rider.height
	
	def check_border_collision(self):
		#LEFT BORDER
		if self.x < self.parent.border.x:
			self.vx = (self.vx * -1) * self.correction
			self.x = self.parent.border.x
		#RIGHT BORDER
		elif self.x + self.diameter > self.parent.border.mx:
			self.vx = (self.vx * -1) * self.correction
			self.x = self.parent.border.mx - self.diameter
		#LOWER BORDER
		if self.y < self.parent.border.y:
			self.parent.game_over("FAIL")
		#UPPER BORDER
		elif self.y + self.diameter > self.parent.border.my:
			self.vy = (self.vy * -1) * self.correction
			self.y = self.parent.border.my - self.diameter
	
	def check_field_collision(self):
		for field in self.parent.fields:
			if self.x + self.diameter > field.x and self.x < field.x + field.width and self.y + self.diameter > field.y and self.y < field.y + field.height:
				field.hit()
				
				if self.previous_x + self.diameter > field.x and self.previous_x <= field.x + field.width:
					#FROM BELLOW
					if self.previous_y + self.diameter <= field.y:
						self.vy = (self.vy * -1) * self.correction
						self.y = field.y - self.diameter
					#FROM ABOVE
					else:
						self.vy = (self.vy * -1) * self.correction
						self.y = field.y + field.height
				if self.previous_y + self.diameter > field.y and self.previous_y < field.y + field.height:
					#FROM LEFT
					if self.previous_x + self.diameter < field.x:
						self.vx = (self.vx * -1) * self.correction
						self.x = field.x - self.diameter
					#FROM RIGHT
					else:
						self.vx = (self.vx * -1) * self.correction
						self.x = field.x + field.width
				
				#UNCERTAIN STATES ...
				if self.previous_x + self.diameter < field.x and self.previous_y + self.diameter < field.y:
					print "FROM LOWER LEFT"
				if self.previous_x > field.x + field.width and self.previous_y + self.diameter < field.y:
					print "FROM LOWER RIGHT"
				if self.previous_x + self.diameter < field.x and self.previous_y > field.y + field.height:
					print "FROM UPPER LEFT"
				if self.previous_x > field.x + field.width and self.previous_y > field.y + field.height:
					print "FROM UPPER RIGHT"

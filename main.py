import pyglet, pyglet.gl, math
from pong import *

class PongWindow(pyglet.window.Window):
	def __init__(self):
		super(PongWindow, self).__init__(500,500)
		self.fps_display = pyglet.clock.ClockDisplay()
		
		self.framerate = 100.0
		self.border = Border(self.width, self.height)
		self.fields = []
		self.ball = None
		self.line = None
		self.rider = None
		self.score = 0
		self.start_x = 0
		self.start_y = 0
		self.final_x = 0
		self.final_y = 0
		self.distance = 0
		self.speed = 0
		self.speed_vec = (0,0)
		self.keys_pressed = pyglet.window.key.KeyStateHandler()
		self.push_handlers(self.keys_pressed)
		pyglet.clock.schedule_interval(self.update, 1/self.framerate)
		self.game_is_over = False
		self.create_game()

	def update(self,dt):
		if not self.game_is_over:
			if self.ball:
				self.ball.move()
			if self.keys_pressed[pyglet.window.key.LEFT]:
				self.rider.move(-10)
			if self.keys_pressed[pyglet.window.key.RIGHT]:
				self.rider.move(10)
	
	def add_to_score(self, score):
		self.score += score
	
	def remove_field(self, field):
		self.fields.remove(field)
		if len(self.fields) == 0:
			self.game_over("WIN")

	def on_draw(self):
			self.clear()
			batch.draw()
			if self.line:
				self.line.line_batch.draw()
			self.fps_display.draw()
		
	def on_mouse_press(self, x, y, button, modifiers):
		if not self.ball:
			self.line = Line(x,y)
			self.start_x = x
			self.start_y = y
	
	def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
		if not self.ball:
			self.line.update(x,y)

	def on_mouse_release(self, x, y, button, modifiers):
		if not self.ball:
			self.final_x = x
			self.final_y = y
			self.distance = math.hypot(self.start_x - self.final_x, self.start_y - self.final_y)
			self.line.stop()
			self.line = None
			
			if self.distance > 100:
				self.speed = 25
			elif self.distance > 75:
				self.speed = 20
			elif self.distance > 50:
				self.speed = 15
			elif self.distance > 25:
				self.speed = 10
			else:
				self.speed = 2
			
			
			self.vxy = [0,0]
			if self.distance > 0:
				self.vxy[0] = (self.start_x - self.final_x) * (self.speed / self.distance)
				self.vxy[1] = (self.start_y - self.final_y) * (self.speed / self.distance)
			
			self.ball = Ball((x,y),self.vxy, self)
	
	def create_game(self):
		for col in range(self.width / 100): #fields are 100px wide and 40px high
			for row in range(4):
				self.fields.append(Field(col*100, self.height-((row+1)*40), random.randrange(0,3), self))
		self.rider = Rider(self)
	
	def game_over(self, type):
		self.game_is_over = True
		FinalText(self, type)
		batch = pyglet.graphics.Batch()

if __name__ == "__main__":
	window = PongWindow()
	pyglet.app.run()